import {useState} from "react";
import styles from "./file-upload.module.css";
import Image from "next/image";
import uploadPng from "../../public/upload.png";

export default function FileUpload({ onFileUploaded }) {
    const maxFileSizeInMBs = 50;
    const [fileTooLarge, setFileToLarge] = useState(false);
    const [preview, setPreview] = useState(null);

    async function parse(file) {
        const res = await fetch(`/api/backend/parse-pdf/`, {
            method: 'POST',
            body: JSON.stringify(file),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return res.json()
    }

    function processFile(e) {
        const file = e.currentTarget.files[0];

        if (file.name.length < 30) {
            setPreview(file.name);
        } else {
            setPreview(file.name.slice(0, 30) + '...');
        }

        if (file.size > 1048576 * maxFileSizeInMBs) {
            setFileToLarge(true);
        }
        else {
            setFileToLarge(false);
            onFileUploaded(file);
        }
    }

    function handleDrop (event) {
        event.preventDefault();
        const droppedFiles = event.dataTransfer.files;
        if (droppedFiles.length > 0) {
            document.getElementById('file').files = droppedFiles;
            processFile({currentTarget:document.getElementById('file')} )
        }
    }

    return <div className={styles['drop-file']}
                onDrop={handleDrop}
                onDragOver={(event) => event.preventDefault()}>
        <div className={styles['drop-file-inner']}>
            <Image className={styles['drop-file-img']} src={uploadPng} alt="upload" />
            <h2 className={styles['drop-file-h2']}>Upload Yor Files</h2>
            <p className={styles['drop-file-p']}>
                Drag and drop PDF file or
                <input className={styles.input} type="file" id="file" name="file" onChange={processFile} accept="application/pdf"/>
            </p>
            <div>
                {
                    fileTooLarge && <p className={styles.error}>
                        {
                            `File "${preview}" is too large. Please choose image smaller then ${maxFileSizeInMBs}MB`
                        }
                    </p>
                }
            </div>
            <div>
                {
                    preview && !fileTooLarge && <p className={styles.success}> {preview} </p>
                }
            </div>
        </div>
    </div>
}