import styles from './footer.module.css'
import Logo from "./logo.js";

export default function Footer() {
    return (
        <footer className={styles.footer}>
            <Logo/>
            <p>© 2023-2024 Arborista.</p>
            <p>All rights reserved</p>
            <p>Serbia, Novi Sad<br/><a href="mailto:arborista.info@gmail.com">arborista.info@gmail.com</a></p>
        </footer>
    )
}
