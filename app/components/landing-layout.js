import Navigation from "./navigation.js";
import Footer from "./footer.js";

export default function LandingLayout({children}) {
    return (
        <div>
            {children}
        </div>
    )
}