import styles from "./button.module.css";


export default function Button({ children, onClick, primary=false, secondary=false, dropdown=false, grey=false, fullwidth=false, disabled=false, enabled=false }) {
    return (
        <button className={
                [
                    styles.button,
                    primary ? styles.primary : '',
                    secondary ? styles.secondary : '',
                    grey ? styles.grey : '',
                    fullwidth ? styles.fullwidth : '',
                    disabled ? styles.disabled : '',
                    enabled ? styles.enabled : '',
                ].join(' ')
            } onClick={onClick}>
            {children}
        </button>
    )
}