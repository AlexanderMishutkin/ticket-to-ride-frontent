'use client'

import Image from "next/image";
import logoSvg from "../../public/logo.svg";
import Link from "next/link.js";
import styles from "./navigation.module.css";
import Button from "./button.js";
import {useEffect, useState} from "react";
import Logo from "./logo.js";
import {useUser} from "@auth0/nextjs-auth0/client";

export default function Navigation() {
    // Mobile
    const [mobileMenu, setMobileMenu] = useState(false)
    const [navBorderStyle, setNavBorderStyle] = useState('')
    const toggleMobileMenu = () => setMobileMenu(!mobileMenu)
    const {user, error, isLoading} = useUser();

    const handleWindowSizeChange = () => {
        if (window.innerWidth / window.innerHeight < 1.0 || window.innerWidth < 768) {
            setMobileMenu(true);
        } else {
            setMobileMenu(false);
        }
    }

    const handleScroll = () => {
        if (window.scrollY > 0) {
            setNavBorderStyle(styles["navigation-bb"]);
        } else {
            setNavBorderStyle('');
        }
    }

    useEffect(() => {
        window.addEventListener('resize', handleWindowSizeChange);
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
            window.removeEventListener('scroll', handleScroll);
        }
    }, []);

    return (
        <nav className={[
            styles.navigation,
            navBorderStyle
        ].join(' ')}>
            <Link href="/">
                <Logo/>
            </Link>
            <span className={styles["nav-links"]}>
                 <div className={styles["nav-left"]}>
                     <Link href="/#reviews">
                         <Button className={styles["nav-link"]}>
                             Reviews
                         </Button>
                     </Link>
                     <Link href="/pricing">
                         <Button className={styles["nav-link"]}>
                             Pricing
                         </Button>
                     </Link>
                     {
                         user ? <Link href="/parser">
                             <Button primary className={styles["nav-link"]}>
                                 Parse
                             </Button>
                         </Link> : <></>
                     }
                 </div>

                 <div className={styles["nav-right"]}>
                     {
                         user
                             ? <>
                                 <Link href="/api/auth/logout">
                                     <Button className={styles["nav-link"]}>
                                         Log Out
                                     </Button>
                                 </Link>
                             </>
                             : <>
                                 <Link href="/api/auth/login">
                                     <Button className={styles["nav-link"]}>
                                         Log In
                                     </Button>
                                 </Link>
                                 <Link href="/api/auth/login">
                                     <Button primary className={styles["nav-link"]}>
                                         Get For Free
                                     </Button>
                                 </Link>
                             </>
                     }
                 </div>
            </span>
        </nav>
    )
}