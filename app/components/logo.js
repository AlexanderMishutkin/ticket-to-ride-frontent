import styles from "./logo.module.css";
import Image from "next/image";

export default function Logo() {
    return (
        <div className={styles.logo}>
            <img className={styles["logo-img"]} src="/logo-tree.svg"  alt="i"/>
            <span className={styles["logo-text"]}>
                Arbor sta
            </span>
        </div>
    )
}
