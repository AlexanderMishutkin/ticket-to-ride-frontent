import styles from '../page.module.css'
import {useEffect, useState} from "react";
import Button from "./button.js";
import Image from "next/image";

export default function SelectMap({onMapSelect}) {
    const [maps, setMaps] = useState([]);

    useEffect(() => {
        fetch('/api/game').then(res => {
            console.log(res)
            res.text().then(txt => {
                console.log(txt)

                if (txt.length === 0) {
                    console.log('No game found')
                } else {
                    fetch('/api/map').then(res => {
                        res.json().then(data => {
                            console.log(data)
                            console.log(onMapSelect)
                            onMapSelect(data)
                        })
                    })
                }
            })
        })

        setMaps(maps => [])
        for (let mapJson of ['map1.json', 'map2.json', 'map3.json']) {
            fetch(`/${mapJson}`)
                .then(res => res.json())
                .then(data => {
                    setMaps(maps => [...maps, data])
                })
        }
    }, [])

    return (
        <div>
            <h2 className={styles['sub-title']}>
                Select Map
            </h2>
            <div className={styles.map}>
                {
                    maps.map((map, index) => {
                            return <div key={index} className={styles.mapCard}>
                                <Image src={map.image.path} width={300} height={300}/>
                                <Button primary={true} href={`/map/${map.id}`} onClick={
                                    () => {
                                        // post to /api/game with map in a body
                                        fetch('/api/game', {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify(map)
                                        }).then(res => {
                                            console.log(res)
                                            console.log(onMapSelect)
                                            onMapSelect(map)
                                        })
                                    }
                                }>
                                    Select "{map.name}"
                                </Button>
                            </div>
                        }
                    )
                }
            </div>
        </div>
    )
}