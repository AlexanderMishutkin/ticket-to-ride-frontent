import styles from "../page.module.css";

export default function PdfViewer({url = '/sample.pdf'}) {

    return (
        <div className={styles.pdfViewer}>
            <iframe src={url} width="100%" height="100%"/>
        </div>
    )
}