import "react-notion/src/styles.css";

import { NotionRenderer } from "react-notion";
import {useEffect, useState} from "react";
import styles from '../page.module.css'
import {hexHash} from "next/dist/shared/lib/hash.js";



export default function NotionViewer({blocks}) {
    const [data, setData] = useState({});

    useEffect(() => {
        // notion-api-worker
        fetch(
            'https://notion-api.splitbee.io/v1/page/ae296e9e0faa49c8b82446a9157a6e6a'
        )
            .then((res) => res.json())
            .then((data) => {
                console.log(data)
                setData(data)
            });
    }, []);

    function createBlock(block) {
        const key = hexHash(block[0]);
        if (block[1] === "Header") {
            return <div className={styles.highlightBoxRight} >
                <h1 key={key} className={styles.highlightRight}>{block[0]}</h1>
            </div>
        }
        return <p key={key} className={styles.p}>{block[0]}</p>
    }

    console.log(blocks);

    return (
        data && <div className={styles.notionViewer}>
            {(!blocks) && <NotionRenderer blockMap={data}/>}
            {blocks && blocks.result.map(createBlock)}
        </div>
    )
}