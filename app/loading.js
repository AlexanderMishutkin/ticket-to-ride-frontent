import Image from 'next/image'
import logoSvg from '../public/logo.svg'
import { Tinos } from "next/font/google";


const tinos = Tinos({
    weight: '400',
    style: 'normal',
    subsets: ['latin'],
    display: 'swap'
})


export default function Loading() {
    return <div>

    </div>
}
