"use client";

import styles from './page.module.css'
import {useEffect, useState} from "react";
import Button from "./components/button.js";
import Image from "next/image";
import {Lora} from 'next/font/google'
import LandingLayout from "./components/landing-layout.js";
import SelectMap from "./components/select-map.js";

const lora = Lora({
    weight: ['400'],
    style: 'normal',
    subsets: ['latin'],
    display: 'swap'
});

export default function Home() {
    const [map, setMap] = useState(null);
    const [name, setName] = useState('');

    return <LandingLayout>
        <div className={styles.main}>
            <h1 className={styles.title}>
                {!map && <>Create New Game</> }
                {map && <>Join Game</>}
            </h1>
            {!map && <SelectMap onMapSelect={setMap}/>}
            <input type="text" placeholder="Your Name" className={styles.input}  value={name} onChange={e => setName(e.target.value)}/>
            <Button primary={true} onClick={() => {
                fetch('/api/player', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        parentName: ''
                    })
                }).then(res => {
                    console.log(res)

                    if (res.status === 200) {
                        window.location.href = '/game?name=' + name
                    }
                })
            } }>Join Game</Button>
        </div>
    </LandingLayout>
}
