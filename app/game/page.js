"use client";

import FileUpload from "../components/file-upload.js";
import styles from "./page.module.css";
import Button from "../components/button.js";
import {useEffect, useState} from "react";
import {Inter} from "next/font/google";
import {useRouter} from "next/dist/client/compat/router.js";
import {useSearchParams} from "next/navigation.js";
import {djb2Hash} from "next/dist/shared/lib/hash.js";

const inter = Inter({
    weight: ['400', '500', '600', '700'],
    style: 'normal',
    subsets: ['latin'],
    display: 'swap'
})


export default function Page() {
    const [map, setMap] = useState(null);
    const [game, setGame] = useState(null);
    const [user, setUser] = useState(null);
    const [selectedCity, setSelectedCity] = useState(null);
    const [otherUsers, setOtherUsers] = useState([]);
    const [userName2Color, setUserName2Color] = useState({});
    const searchParams = useSearchParams();
    const userName = searchParams.get('name');
    const colorId2Color = {
        0: 'red',
        1: 'blue',
        2: 'green',
        3: 'yellow',
        4: 'purple',
        5: 'orange',
        6: 'pink',
        7: 'black',
        8: 'white'
    }

    const possibleStepBoxShadow = "\n" +
        "    0 0 0 0.2em #f6e05e, \n" +
        "    0 0 0.4em 0.4em rgba(246, 224, 94, 0.8), \n" +
        "    0 0 1em 1em rgba(246, 224, 94, 0.6), \n" +
        "    0 0 2em 2em rgba(246, 224, 94, 0.4)"

    function getStrokeDashArray(length, N) {
        const dashLength = length / (2 * N - 1);
        return `${dashLength},${dashLength}`;
    }

    function getCityColor(city) {
        if (selectedCity && selectedCity.name === city.name) {
            return 'yellow';
        }
        let owner = game?.map?.stations?.find(station => station.city.name === city.name)?.owner;
        if (owner) {
            return userName2Color[owner.name];
        }
        return 'black';
    }

    function stretchOccupiedByPlayer(stretch) {
        if (!game || !game.map || !game.map.stretches) {
            return false;
        }
        return game.map.stretches.find(
            s => s.potentialStretch.firstCity.name === stretch.firstCity.name &&
            s.potentialStretch.secondCity.name === stretch.secondCity.name)?.owner.name;
    }

    function isClosedCardInPossibleSteps() {
        if (selectedCity || !user || !game || !game.possibleSteps || user.name !== game.currentUser?.name) {
            return false;
        }
        return game.possibleSteps.some(step => {
            return step.type === "TAKE_FIRST_CLOSED_CARD" || step.type === "TAKE_SECOND_CLOSED_CARD"
        });
    }

    function isOpenCardInPossibleSteps() {
        if (selectedCity || !user || !game || !game.possibleSteps || user.name !== game.currentUser?.name) {
            return false;
        }
        return game.possibleSteps.some(step => {
            return step.type === "TAKE_FIRST_OPEN_CARD" || step.type === "TAKE_SECOND_OPEN_CARD"
        });
    }

    function isCityInPossibleSteps(city) {
        if ((selectedCity && selectedCity.name !== city.name) || !user || !game || !game.possibleSteps || user.name !== game.currentUser?.name) {
            return false;
        }
        return game.possibleSteps.some(step => {
            return step.relatedCity?.name === city.name
        });
    }

    function isStretchInPossibleSteps(stretch) {
        if (selectedCity || !user || !game || !game.possibleSteps || user.name !== game.currentUser?.name) {
            return false;
        }
        return game.possibleSteps.some(step => {
            return step.relatedStretch?.potentialStretch.firstCity.name === stretch.firstCity.name && step.relatedStretch?.potentialStretch.secondCity.name === stretch.secondCity.name
        });
    }

    useEffect(() => {
        fetch('/api/map').then(res => {
            res.json().then(data => {
                setMap(data)
            })
        })

        setInterval(() => {
            fetch('/api/game').then(res => {
                res.json().then(data => {
                    setGame(data)
                    console.log(data)
                })
            })
        }, 1000)
    }, [])

    useEffect(() => {
        if (!game) {
            return
        }
        for (let i = 0; i < game.users.length; i++) {
            if (game.users[i].name === userName) {
                setUser(game.users[i])
                setOtherUsers(game.users.filter((user, index) => index !== i))
            }
        }

        setUserName2Color(userName2Color => {
            for (let i = 0; i < game.users.length; i++) {
                userName2Color[game.users[i].name] = colorId2Color[djb2Hash(game.users[i].name) % 9]
            }
            userName2Color['AAA'] = colorId2Color[0]
            userName2Color['BBB'] = colorId2Color[1]
            return userName2Color
        })

        setMap(map => {
            if (!map) {
                return map
            }
            map.stations = game.map.stations;
            map.stretches = game.map.stretches;
            return map
        })
    }, [game])

    return (
        <div className={styles.main} style={
            {
                backgroundImage: `url(/game-${!game ? 1 : game.users.length}.png)`
            }
        }>
            <div className={styles.field}>

            </div>
            <div>
                {
                    otherUsers && otherUsers.length > 0 &&
                    <div className={styles.player1} style={
                        {
                            backgroundColor: userName2Color[otherUsers[0].name]
                        }
                    }>
                        {
                            <>{otherUsers[0].name}</>
                        }
                    </div>
                }
                {otherUsers && otherUsers.length > 1 && <div className={styles.player2} style={
                    {
                        backgroundColor: userName2Color[otherUsers[1].name]
                    }
                }>
                    {
                        <>{otherUsers[1].name}</>
                    }
                </div>}
                {otherUsers && otherUsers.length > 2 && <div className={styles.player3} style={
                    {
                        backgroundColor: userName2Color[otherUsers[2].name]
                    }
                }>
                    {
                        <>{otherUsers[2].name}</>
                    }
                </div>}
            </div>
            {(game && !game.currentUser && !game.finished) && <dvi className={styles.start}>
                <Button primary={true} onClick={() => {
                    fetch('/api/game/start', {
                        method: 'POST'
                    }).then(res => {
                        console.log(res)
                        fetch('/api/game').then(res => {
                            res.json().then(data => {
                                setGame(data)
                            })
                        })
                    })
                }}>Start Game</Button>
            </dvi>}
            {
                game &&
                <div className={styles.actions}>
                    <Button primary={true} secondary={true} onClick={() => {
                        fetch('/api/game/stop', {
                            method: 'POST'
                        }).then(res => {
                            console.log(res)
                            window.location.href = '/'
                        })

                    }
                    }>Remove Game</Button>
                </div>
            }
            {
                map && <div className={styles.map} style={
                    {
                        backgroundImage: `url(${map.image.path})`,
                        height: map.image.size.h,
                        width: map.image.size.w
                    }
                }>
                    {
                        map.cities.map((city, index) => {
                            return <div key={index} className={styles.city} style={
                                {
                                    left: city.coordinates.x,
                                    top: city.coordinates.y,
                                    boxShadow: isCityInPossibleSteps(city) ? possibleStepBoxShadow : 'none',
                                    backgroundColor: getCityColor(city)
                                }
                            } onClick={
                                () => {
                                    if (city.name === selectedCity?.name) {
                                        setSelectedCity(null)
                                        return
                                    }
                                    if (!isCityInPossibleSteps(city)) {
                                        return
                                    }
                                    setSelectedCity(city)
                                }
                            }>
                                <span>{city.name}</span>
                                {city.name === selectedCity?.name && <div className={styles.cityPossibleSteps}>
                                    {
                                        game.possibleSteps.filter(
                                            step => step.relatedCity?.name === selectedCity?.name
                                        ).map(ps=>ps.relatedCards?.map((card, index) => {
                                            return <div key={index} className={styles.cityPSCard} style={
                                                {
                                                    backgroundColor: colorId2Color[card.colorId],
                                                    boxShadow: possibleStepBoxShadow
                                                }
                                            } onClick={
                                                () => {
                                                    fetch('/api/step', {
                                                        method: 'POST',
                                                        headers: {
                                                            'Content-Type': 'application/json'
                                                        },
                                                        body: JSON.stringify(
                                                            game.possibleSteps.find(
                                                                step => step.relatedCity?.name === selectedCity?.name &&
                                                                step.relatedCards.find(
                                                                    relatedCard => relatedCard.colorId === card.colorId
                                                                )
                                                            )
                                                        )
                                                    }).then(res => {
                                                        console.log(res)
                                                        setSelectedCity(null)
                                                    })
                                                }
                                            }>
                                            </div>
                                        }))
                                    }
                                </div>}
                            </div>
                        })
                    }
                    <svg height={
                        map.image.size.h
                    } width={
                        map.image.size.w
                    } style={
                        {
                            position: "absolute",
                            top: 0, left: 0
                        }
                    }>
                        <defs>
                            <filter id="golden-shadow" x="-50%" y="-50%" width="200%" height="200%">
                                <feOffset in="SourceAlpha" dx="0" dy="0" result="offset1"/>
                                <feGaussianBlur in="offset1" stdDeviation="5" result="blur1"/>
                                <feFlood flood-color="#f6e05e" result="color"/>
                                <feComposite in="color" in2="blur1" operator="in" result="shadow1"/>
                                <feOffset in="SourceAlpha" dx="0" dy="0" result="offset2"/>
                                <feGaussianBlur in="offset2" stdDeviation="10" result="blur2"/>
                                <feFlood flood-color="rgba(246, 224, 94, 0.8)" result="color"/>
                                <feComposite in="color" in2="blur2" operator="in" result="shadow2"/>
                                <feOffset in="SourceAlpha" dx="0" dy="0" result="offset3"/>
                                <feGaussianBlur in="offset3" stdDeviation="15" result="blur3"/>
                                <feFlood flood-color="rgba(246, 224, 94, 0.6)" result="color"/>
                                <feComposite in="color" in2="blur3" operator="in" result="shadow3"/>
                                <feOffset in="SourceAlpha" dx="0" dy="0" result="offset4"/>
                                <feGaussianBlur in="offset4" stdDeviation="20" result="blur4"/>
                                <feFlood flood-color="rgba(246, 224, 94, 0.4)" result="color"/>
                                <feComposite in="color" in2="blur4" operator="in" result="shadow4"/>
                                <feMerge>
                                    <feMergeNode in="shadow1"/>
                                    <feMergeNode in="shadow2"/>
                                    <feMergeNode in="shadow3"/>
                                    <feMergeNode in="shadow4"/>
                                    <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                        </defs>
                        {
                            map.potentialStretches.map((stretch, index) => {
                                return <line key={index} x1={
                                    map.cities.find(city => city.name === stretch.firstCity.name).coordinates.x
                                } y1={
                                    map.cities.find(city => city.name === stretch.firstCity.name).coordinates.y
                                } x2={
                                    map.cities.find(city => city.name === stretch.secondCity.name).coordinates.x
                                } y2={
                                    map.cities.find(city => city.name === stretch.secondCity.name).coordinates.y
                                } style={
                                    !stretchOccupiedByPlayer(stretch) ?
                                    {
                                        stroke: colorId2Color[stretch.colorId],
                                        strokeWidth: "1em",
                                        strokeDasharray: getStrokeDashArray(
                                            Math.sqrt(
                                                Math.pow(
                                                    map.cities.find(city => city.name === stretch.firstCity.name).coordinates.x -
                                                    map.cities.find(city => city.name === stretch.secondCity.name).coordinates.x, 2) +
                                                Math.pow(map.cities.find(city => city.name === stretch.firstCity.name).coordinates.y -
                                                    map.cities.find(city => city.name === stretch.secondCity.name).coordinates.y, 2)
                                            ), stretch.length)
                                    } :
                                        {
                                            stroke: userName2Color[stretchOccupiedByPlayer(stretch)],
                                            strokeWidth: "0.8em",
                                        }
                                }
                                             filter={isStretchInPossibleSteps(stretch) ? "url(#golden-shadow)" : "none"}
                                             onClick={
                                    () => {
                                        if (!isStretchInPossibleSteps(stretch)) {
                                            return
                                        }
                                        fetch('/api/step', {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify(
                                                game.possibleSteps.find(
                                                    step => step.relatedStretch?.potentialStretch.firstCity.name === stretch.firstCity.name &&
                                                    step.relatedStretch?.potentialStretch.secondCity.name === stretch.secondCity.name
                                                )
                                            )
                                        }).then(res => {
                                            console.log(res)
                                        })
                                    }
                                }
                                />
                            })
                        }
                    </svg>
                </div>
            }
            {user && <div className={styles.hand}>
                <div className={styles.cards}>
                    {
                        user.cards.map((card, index) => {
                            return <div key={index} className={styles.card} style={
                                {
                                    backgroundColor: colorId2Color[card.colorId]
                                }
                            }>
                                Ticket To Ride
                            </div>
                        })
                    }
                </div>
                <div className={styles.tasks}>
                    {
                        user.tasks.map((task, index) => {
                            return <div key={index} className={styles.task}>
                                {task.firstCity.name}
                                <div className={styles.award}>
                                    {task.award}
                                </div>
                                {task.secondCity.name}
                            </div>
                        })
                    }
                </div>
                <div className={styles.trains}>
                    {
                        user && Array.from({length: user.trainsLeft}).map((_, index) => {
                            return <div key={index} className={styles.train} style={{
                                backgroundColor: userName2Color[user.name]
                            }}>

                            </div>
                        })
                    }
                </div>
            </div>}
            <div className={styles.deckenopen}>
                {
                    game && game.cardsInDeck && <div className={styles.deck}
                        style={
                            {
                                boxShadow: isClosedCardInPossibleSteps() ? possibleStepBoxShadow : 'none'
                            }
                        }
                        onClick={
                            () => {
                                if (!isClosedCardInPossibleSteps()) {
                                    return
                                }
                                fetch('/api/step', {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify(
                                            game.possibleSteps.find(
                                                step => step.type === "TAKE_FIRST_CLOSED_CARD" || step.type === "TAKE_SECOND_CLOSED_CARD"
                                            )
                                        )
                                    }
                                )
                            }
                        }
                    >
                        Deck: {game.cardsInDeck.length}
                    </div>
                }
                {
                    game && game.openCards && game.openCards.map((card, index) => {
                        return <div className={styles.openCards}
                                    style={{
                                        backgroundColor: colorId2Color[card.colorId],
                                        boxShadow: isOpenCardInPossibleSteps() ? possibleStepBoxShadow : 'none'
                                    }}
                                    onClick={
                                        () => {
                                            if (!isClosedCardInPossibleSteps()) {
                                                return
                                            }
                                            fetch('/api/step', {
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/json'
                                                    },
                                                    body: JSON.stringify(
                                                        game.possibleSteps.filter(
                                                            step => step.type === "TAKE_FIRST_OPEN_CARD" || step.type === "TAKE_SECOND_OPEN_CARD"
                                                        ).map(
                                                            step => {
                                                                return {
                                                                    ...step,
                                                                    relatedCards: [card]
                                                                }
                                                            }
                                                        ).find(a=>true)
                                                    )
                                                }
                                            )
                                        }
                                    }
                        >
                            Ticket To Ride
                        </div>
                    })
                }
            </div>
            <div className={styles.leaderboard}>
                {
                    game?.finished ? <h1>Final Leaderboard</h1> : <h1>Leaderboard (Ongoing)</h1>
                }
                {
                    game?.users.sort((a, b) => b.score - a.score).map((user, index) => {
                        return <div key={index} className={styles.userPosition} style={{
                            backgroundColor: userName2Color[user.name]
                        }}>
                                {user.score} / {user.name}
                        </div>
                    })
                }
            </div>
        </div>
    )
}

