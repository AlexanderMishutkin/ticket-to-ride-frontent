module.exports = {
    output: "standalone",
    images: {
        formats: ['image/webp'],
    },
    compiler: {
        styledComponents: true,
    },
    experimental: {
        proxyTimeout: 1000 * 1200,
    },
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: `http://localhost:8080/:path*`,
                // destination: `http://arborista-backend:5000/:path*`,
                // destination: `http://54.93.222.69:5000/:path*`,
            },
        ]
    },
}